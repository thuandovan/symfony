<?php
namespace ShopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;


class DefaultController extends Controller
{
	public function indexAction()
	{	
		// return $this->redirectToRoute('lucky_number');
		// $bug = new RedirectResponse('home');
		// dump($bug); exit;
		return $this->render('ShopBundle:Homepage:index.html.twig');
	}
}
