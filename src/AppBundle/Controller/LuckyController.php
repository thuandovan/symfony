<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\RouteCollection;

class LuckyController extends Controller
{

    /**
     * @Route("/luckynumber_anotations")
     */
    public function numberAction()
    {

            $number = mt_rand(0, 100);

        // return new Response(
        //     '<html><body>Lucky number: '.$number.'</body></html>'
        // );
            return $this->render('luckynumber/luckynumber.html.twig',['luckynumber'=>$number]);
    }
}
