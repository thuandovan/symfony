<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('game_homepage', new Route('/game', array(
    '_controller' => 'GameBundle:Default:index',
)));

return $collection;
	